# TP4 : DHCP
### 👩‍💻= où se trouve la réponse
## I. DHCP Client
🌞 Déterminer 
* l'adresse du serveur DHCP
* l'heure exacte à laquelle vous avez obtenu votre bail DHCP
* l'heure exacte à laquelle il va expirer  
_commande utilisée : `ipconfig /all`_
```
  👩‍💻 Bail obtenu. . . . . . . . . . . . . . : vendredi 27 octobre 2023 08:49:42
  👩‍💻 Bail expirant. . . . . . . . . . . . . : samedi 28 octobre 2023 08:49:36
   Passerelle par défaut. . . . . . . . . : 10.33.79.254
  👩‍💻 Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
   IAID DHCPv6 . . . . . . . . . . . : 166770206
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-B7-05-71-A0-36-BC-2E-A1-1A
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   ```
🌞 Capturer un échange DHCP
* forcer votre OS à refaire un échange DHCP complet
```
👩‍💻C:\Users\erika> ipconfig /release
```
* utiliser Wireshark pour capturer les 4 trames DHCP  
👩‍💻[capture dhcp DORA client](./tp4_dhcp_client.pcapng)

🌞 Analyser la capture Wireshark

* parmi ces 4 trames, laquelle contient les informations proposées au client ?

    OFFER ET ACK

## II. Serveur DHCP
### 3. Setup topologie
🌞 Preuve de mise en place
* depuis dhcp.tp4.b1, envoyer un ping vers un nom de domaine public
```
[erika@dhcp ~]$  👩‍💻ping google.com
PING google.com (172.217.19.142) 56(84) bytes of data.
64 bytes from mrs08s04-in-f14.1e100.net (172.217.19.142): icmp_seq=1 ttl=110 time=26.0 ms
64 bytes from mrs08s04-in-f14.1e100.net (172.217.19.142): icmp_seq=2 ttl=110 time=27.0 ms
64 bytes from mrs08s04-in-f14.1e100.net (172.217.19.142): icmp_seq=3 ttl=110 time=28.1 ms
^C64 bytes from 172.217.19.142: icmp_seq=4 ttl=110 time=25.2 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 9384ms
rtt min/avg/max/mdev = 25.247/26.599/28.115/1.077 ms
```
* depuis node2.tp4.b1, envoyer un ping vers un nom de domaine public 
```
[erika@node2tp4 ~]$  👩‍💻ping google.com
PING google.com (172.217.19.142) 56(84) bytes of data.
64 bytes from mrs08s04-in-f14.1e100.net (172.217.19.142): icmp_seq=1 ttl=110 time=30.2 ms
64 bytes from mrs08s04-in-f14.1e100.net (172.217.19.142): icmp_seq=2 ttl=110 time=43.5 ms
^C64 bytes from 172.217.19.142: icmp_seq=3 ttl=110 time=33.9 ms

--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 6371ms
```
* depuis node2.tp4.b1, un traceroute vers une IP publique pour montrer qu

```
[erika@node2tp4 ~]$  👩‍💻traceroute 1.1.1.1
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  localhost.localdomain (10.4.1.12)  3048.653 ms !H  3048.100 ms !H  3048.090 ms !H
```
## 4. Serveur DHCP
🌞 Rendu  
👩‍💻`dnf -y installer le serveur DHCP`

👩‍💻`sudo nano /etc/dhcp/dhcpd.conf`

👩‍💻`sudo cat /etc/dhcp/dhcpd.conf`
```
 create new
specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.4.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.4.1.137 10.4.1.237;
    # specify broadcast address
    option broadcast-address 10.4.1.255;
    # specify gateway
    option routers 10.4.1.1;
}
```
```
[erika@dhcp ~]$ 👩‍💻sudo systemctl enable --now dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
```
## 5. Client DHCP
🌞 **Test !**  
👩‍💻`sudo touch ifcfg-enp0s3`   
👩‍💻`sudo nano ifcfg-enp0s3`
```
DEVICE=enp0s8
BOOTPROTO=dhcp
ONBOOT=yes
```
👩‍💻_commandes effectuées_ :
```
sudo nmcli con reload
sudo nmcli con up "System enp0s3"
sudo systemctl restart NetworkManager
```
👩‍💻`ip a`
```
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4b:cf:e3 brd ff:ff:ff:ff:ff:ff
    inet 10.4.1.137/24 brd 10.4.1.255 scope global dynamic noprefixroute enp0s3
```

🌞 Prouvez que

```
Nov 08 18:00:30 dhcp dhcpd[759]: DHCPDISCOVER from 08:00:27:4b:cf:e3 via enp0s3
Nov 08 18:00:34 dhcp dhcpd[759]: DHCPOFFER on 10.4.1.137 to 08:00:27:4b:cf:e3 (node1) via enp0s3
Nov 08 18:00:34 dhcp dhcpd[759]: DHCPDISCOVER from 08:00:27:4b:cf:e3 (node1) via enp0s3
Nov 08 18:00:34 dhcp dhcpd[759]: DHCPOFFER on 10.4.1.137 to 08:00:27:4b:cf:e3 (node1) via enp0s3
Nov 08 18:00:34 dhcp dhcpd[759]: DHCPREQUEST for 10.4.1.137 (10.4.1.253) from 08:00:27:4b:cf:e3 (node1) via enp0s3
Nov 08 18:00:34 dhcp dhcpd[759]: DHCPACK on 10.4.1.137 to 08:00:27:4b:cf:e3 (node1) via enp0s3
```
🌞 Bailer le serveur DHCP

👩‍💻`cat /var/lib/dhcpd/dhcpd.leases`
```
bail 10.4.1.137 {
  commence le 3 10/11/2023 20:15:05 ;
  se termine le 3 10/11/2023 20:25:05 ;
  cltt 3 2023/11/10 20:15:05;
  état de liaison actif ;
  prochain état de liaison libre ;
  rembobiner l'état de liaison gratuitement ;
  Ethernet matériel 08:00:27:4b:cf:e3 ;
  identifiant "\001\010\000'K\317\343" ;
  nom d'hôte client "node1tp4" ;
}
```
## 6. Option DHCP

🌞Nouvelle conf !
* montrez la nouvelle conf : (avec la commande cat)
 ```
[erika@dhcp ~]$ 👩‍💻sudo cat /etc/dhcp/dhcpd.conf
# créer un nouveau
# spécifiez le nom de domaine
option nom de domaine "srv.world" ;
# spécifiez le nom d'hôte ou l'adresse IP du serveur DNS
option serveurs de noms de domaine 8.8.8.8 ;
# durée de location par défaut
durée de location par défaut 21 600 ;
# durée maximale du bail
durée maximale de location 22 000 ;
# ce serveur DHCP doit être déclaré valide
faisant autorité;
# spécifiez l'adresse réseau et le masque de sous-réseau
sous-réseau 10.4.1.0 masque de réseau 255.255.255.0 {
    # spécifiez la plage d'adresses IP du bail
    plage dynamique-bootp 10.4.1.137 10.4.1.237 ;
    # spécifier l'adresse de diffusion
    option adresse de diffusion 10.4.1.255 ;
    # spécifiez la passerelle
    routeurs d'options 10.4.1.254 ;
}
```
*  redémarrage du service DHCP (sudo systemctl restart dhcpd)  
👩‍💻`sudo systemctl`

🌞 Testez !

* redemandez une IP avec le client node1.tp4.b1   
👩‍💻`sudo dhclient -r enp0s3`   
👩‍💻`sudo dhclient enp0s3`

```
[erika@node1tp4 ~]$ 👩‍💻cat /etc/resolv.conf
# Généré par NetworkManager
recherche sur srv.world
google.com 8.8.8.8
```

* redemandez une IP avec le client node1.tp4.b1

```
[erika@node1tp4 ~]$ 👩‍💻ip rs
par défaut via 10.4.1.254 dev enp0s3
défaut via 10.4.1.254 dev enp0s3 proto dhcp src 10.4.1.137 métrique 100
10.4.1.0/24 dev enp0s3 proto kernel scope link src 10.4.1.137 métrique 100
```
* prouvez que vous avez un accès Internet après cet échange DHCP

```
[erika@node1tp4 ~]$ 👩‍💻ping google.com
PING google.com (172.217.18.206) 56(84) octets de données.
64 octets de ham02s14-in-f206.1e100.net (172.217.18.206) : icmp_seq=1 ttl=115 time=19,8 ms
64 octets de par10s38-in-f14.1e100.net (172.217.18.206) : icmp_seq=2 ttl=115 time=19,3 ms
^C64 octets de 172.217.18.206 : icmp_seq=4 ttl=115 time=20,0 ms
--- statistiques ping de google.com ---
👩‍💻4 paquets transmis, 4 reçus, 0 % de perte de paquets, temps 9377 ms
trt min/moy/max/mdev = 19,321/19,743/19,952/0,256 ms
```
