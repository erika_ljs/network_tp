## TP Reseau 1
### 👩‍💻= où se trouve la réponse
### 1. Affichage d'informations sur la pile TCP/IP locale
🌞Affichez les infos des cartes réseau de votre PC:

```
👩‍💻PS C:\Users\erika> ipconfig /all

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   👩‍💻Description. . . . . . . . . . . . . . : Intel(R) Ethernet Connection (16) I219-LM
   👩‍💻Adresse physique . . . . . . . . . . . : A0-36-BC-2E-A1-1A
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   👩‍💻Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   👩‍💻Adresse physique . . . . . . . . . . . : F0-B6-1E-0A-D0-34
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::f55:8a87:642e:501d%16(préféré)
   👩‍💻Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.120(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 12 octobre 2023 09:02:23
   Bail expirant. . . . . . . . . . . . . : vendredi 13 octobre 2023 09:02:19
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 166770206
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-B7-05-71-A0-36-BC-2E-A1-1A
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

```

🌞Affichez votre passerelle
```
PS C:\Users\erika> ipconfig /all

Carte réseau sans fil Wi-Fi :

   👩‍💻 Passerelle par défaut. . . . . . . . . : 10.33.51.254
   
```
🌞Déterminer la MAC de la passerelle

```
👩‍💻PS C:\Users\erika> arp -a

Interface : 10.33.48.120 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.48.14           e4-b3-18-48-36-68     dynamique
  10.33.48.106          f4-c8-8a-6f-04-05     dynamique
  10.33.48.124          70-d8-23-d5-d2-5c     dynamique
  10.33.49.84           f4-6d-3f-32-3f-5c     dynamique
 👩‍💻 10.33.51.254          7c-5a-1c-cb-fd-a4     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
### 2. Modifications des informations

#### A. Modification d'adresse IP (partie 1)

🌞Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : F0-B6-1E-0A-D0-34
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::f55:8a87:642e:501d%16(préféré)
   👩‍💻Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.11(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 166770206
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-B7-05-71-A0-36-BC-2E-A1-1A
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

🌞 Il est possible que vous perdiez l'accès à internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

```
👩‍💻PS C:\Users\erika> ping 8.8.8.8.
La requête Ping n’a pas pu trouver l’hôte 8.8.8.8.. Vérifiez le nom et essayez à nouveau.

```
connexion internet interompue car j'avais la même adresse ip que quelqu'un

🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée
```
PS C:\Users\erika> ipconfig /all

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Ethernet Connection (16) I219-LM
   Adresse physique . . . . . . . . . . . : A0-36-BC-2E-A1-1A
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::d9d9:e6e3:7573:874c%5(préféré)
   👩‍💻Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.11(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 211826364
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-B7-05-71-A0-36-BC-2E-A1-1A
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```


🌞 Déterminer l'adresse MAC de votre correspondant
```
Interface : 10.10.10.11 --- 0x5
  Adresse Internet      Adresse physique      Type
 👩‍💻 10.10.10.12           30-9c-23-fc-e5-81     dynamique
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```
  ### 4. Petit chat privé
🌞 sur le PC serveur
  ```
  PS C:\Users\Nocthis\Desktop\netcat-win32-1.11\netcat-1.11> .\nc64.exe -l -p 8888
  ```

  🌞 sur le PC client 
  ```
  PS C:\Users\erika\Downloads\netcat-win32-1.11 (1)\netcat-1.11> .\nc.exe 10.10.10.12 8888
gg
ca va ?
ca marche enfin cest cool
on va lancer un buisness
lets go je te suis
yeeeeeah
bon on poursuis le tp.....
```
## 6. Utilisation d'un des deux comme gateway
🌞Tester l'accès internet
```
PS C:\Users\erika> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=15 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=13 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=14 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=13 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 13ms, Maximum = 15ms, Moyenne = 13ms
```

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP
🌞Exploration du DHCP, depuis votre PC

* afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV

* Trouver la date d'expiration de votre bail DHCP

_commande utilisée : ipconfig /all_
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : F0-B6-1E-0A-D0-34
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::f55:8a87:642e:501d%16(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.120(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 16 octobre 2023 09:01:19
   👩‍💻Bail expirant. . . . . . . . . . . . . : mardi 17 octobre 2023 09:01:19
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   👩‍💻Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 166770206
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-B7-05-71-A0-36-BC-2E-A1-1A
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
  ```
  ### 2. DNS
  🌞** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**
  _commande utilisée : ipconfig /all_
```
Carte réseau sans fil Wi-Fi :
(...)
   👩‍💻Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
```
🌞 Utiliser, en ligne de commande l'outil nslookup
* pour google.com

```
C:\Users\erika> nslookup google.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:818::200e
          142.250.179.110
 ```


* pour ynov.com
```
C:\Users\erika> nslookup ynov.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          2606:4700:20::ac43:4ae2
          104.26.10.233
          172.67.74.226
          104.26.11.233
```

* déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes

      8.8.8.8

#### Reverse lookup : 

* pour l'adresse 231.34.113.12
```
C:\Users\erika> nslookup 231.34.113.12 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 231.34.113.12 : Non-existent domain
```
* pour l'adresse 78.34.2.17
```
PS C:\Users\erika> ping 78.34.2.17

Envoi d’une requête 'Ping'  78.34.2.17 avec 32 octets de données :
Réponse de 78.34.2.17 : octets=32 temps=34 ms TTL=55
Réponse de 78.34.2.17 : octets=32 temps=33 ms TTL=55
Réponse de 78.34.2.17 : octets=32 temps=33 ms TTL=55
Réponse de 78.34.2.17 : octets=32 temps=31 ms TTL=55

Statistiques Ping pour 78.34.2.17:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 31ms, Maximum = 34ms, Moyenne = 32ms
```
