# 1. Echange ARP

### 👩‍💻= où se trouve la réponse
#### 🌞Générer des requêtes ARP

* effectuer un ping d'une machine à l'autre  
_machine John_
```
[erika@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
: icmp_seq=7 ttl=64 time=0.719 ms
64 bytes from 10.3.1.12: icmp_seq=8 ttl=64 time=0.724 ms
64 bytes from 10.3.1.12: icmp_seq=9 ttl=64 time=0.666 ms
64 bytes from 10.3.1.12: icmp_seq=10 ttl=64 time=0.717 ms

--- 10.3.1.12 ping statistics ---
21 packets transmitted, 21 received, 0% packet loss, time 20401ms
rtt min/avg/max/mdev = 0.592/0.777/1.620/0.265 ms
```

_Machine Marcel_
```
[erika@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.12 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.544 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.583 ms

--- 10.3.1.11 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9181ms
rtt min/avg/max/mdev = 0.418/0.707/1.119/0.198 ms
```

* repérer l'adresse MAC de Marcel dans la table ARP de John
```
[erika@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:06 REACHABLE
10.3.1.12 dev enp0s3 lladdr 👩‍💻08:00:27:f1:08:24 STALE
```
* repérer l'adresse MAC de john dans la table ARP de marcel
```
[erika@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:06 REACHABLE
10.3.1.11 dev enp0s3 lladdr 👩‍💻08:00:27:a6:11:26 STALE
```
................................  
Prouvez que l'info est correcte 

* une commande pour voir la MAC de marcel dans la table ARP de john
```
[erika@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:06 REACHABLE
10.3.1.12 dev enp0s3 lladdr 👩‍💻08:00:27:f1:08:24 STALE
```

* une commande pour afficher la MAC de marcel, depuis marcel
``` 
[erika@localhost ~]$ ip addr

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 👩‍💻08:00:27:f1:08:24 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef1:824/64 scope link
       valid_lft forever preferred_lft forever
```
### 2. Analyse de trames
🌞Analyse de trames
```
[erika@localhost ~]$ sudo tcpdump -i enp0s3 -c 10 -w mon_fichier.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
```
[capture réseau ARP request/reply](./tp3_arp.pcapng)


# II. Routage
## 1. Mise en place du routage
🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping

Machine John 
```
👩‍💻[erika@localhost network-scripts]$ ip route add
Usage: ip route { list | flush } SELECTOR
       ip route save SELECTOR
       ip route restore
       ip route showdump
       ip route get [ ROUTE_GET_FLAGS ] ADDRESS
                            [ from ADDRESS iif STRING ]
                            [ oif STRING ] [ tos TOS ]
                            [ mark NUMBER ] [ vrf NAME ]
                            [ uid NUMBER ] [ ipproto PROTOCOL ]
                            [ sport NUMBER ] [ dport NUMBER ]
       ip route { add | del | change | append | replace } ROUTE
SELECTOR := [ root PREFIX ] [ match PREFIX ] [ exact PREFIX ]
            [ table TABLE_ID ] [ vrf NAME ] [ proto RTPROTO ]
            [ type TYPE ] [ scope SCOPE ]
ROUTE := NODE_SPEC [ INFO_SPEC ]
NODE_SPEC := [ TYPE ] PREFIX [ tos TOS ]
             [ table TABLE_ID ] [ proto RTPROTO ]
             [ scope SCOPE ] [ metric METRIC ]
             [ ttl-propagate { enabled | disabled } ]
INFO_SPEC := { NH | nhid ID } OPTIONS FLAGS [ nexthop NH ]...
NH := [ encap ENCAPTYPE ENCAPHDR ] [ via [ FAMILY ] ADDRESS ]
      [ dev STRING ] [ weight NUMBER ] NHFLAGS
FAMILY := [ inet | inet6 | mpls | bridge | link ]
OPTIONS := FLAGS [ mtu NUMBER ] [ advmss NUMBER ] [ as [ to ] ADDRESS ]
           [ rtt TIME ] [ rttvar TIME ] [ reordering NUMBER ]
           [ window NUMBER ] [ cwnd NUMBER ] [ initcwnd NUMBER ]
           [ ssthresh NUMBER ] [ realms REALM ] [ src ADDRESS ]
           [ rto_min TIME ] [ hoplimit NUMBER ] [ initrwnd NUMBER ]
           [ features FEATURES ] [ quickack BOOL ] [ congctl NAME ]
           [ pref PREF ] [ expires TIME ] [ fastopen_no_cookie BOOL ]
TYPE := { unicast | local | broadcast | multicast | throw |
          unreachable | prohibit | blackhole | nat }
TABLE_ID := [ local | main | default | all | NUMBER ]
SCOPE := [ host | link | global | NUMBER ]
NHFLAGS := [ onlink | pervasive ]
RTPROTO := [ kernel | boot | static | NUMBER ]
PREF := [ low | medium | high ]
TIME := NUMBER[s|ms]
BOOL := [1|0]
FEATURES := ecn
ENCAPTYPE := [ mpls | ip | ip6 | seg6 | seg6local | rpl | ioam6 | xfrm ]
ENCAPHDR := [ MPLSLABEL | SEG6HDR | SEG6LOCAL | IOAM6HDR | XFRMINFO ]
SEG6HDR := [ mode SEGMODE ] segs ADDR1,ADDRi,ADDRn [hmac HMACKEYID] [cleanup]
SEGMODE := [ encap | encap.red | inline | l2encap | l2encap.red ]
SEG6LOCAL := action ACTION [ OPTIONS ] [ count ]
ACTION := { End | End.X | End.T | End.DX2 | End.DX6 | End.DX4 |
            End.DT6 | End.DT4 | End.DT46 | End.B6 | End.B6.Encaps |
            End.BM | End.S | End.AS | End.AM | End.BPF }
OPTIONS := OPTION [ OPTIONS ]
OPTION := { flavors FLAVORS | srh SEG6HDR | nh4 ADDR | nh6 ADDR | iif DEV | oif DEV |
            table TABLEID | vrftable TABLEID | endpoint PROGNAME }
FLAVORS := { FLAVOR[,FLAVOR] }
FLAVOR := { psp | usp | usd | next-csid }
IOAM6HDR := trace prealloc type IOAM6_TRACE_TYPE ns IOAM6_NAMESPACE size IOAM6_TRACE_SIZE
XFRMINFO := if_id IF_ID [ link_dev LINK ]
ROUTE_GET_FLAGS := [ fibmatch ]
```
```
[erika@localhost network-scripts]$👩‍💻 ip route show
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
```

Macine Marcel 
```
👩‍💻[erika@localhost ~]$ ip route add
Usage: ip route { list | flush } SELECTOR
       ip route save SELECTOR
       ip route restore
       ip route showdump
       ip route get [ ROUTE_GET_FLAGS ] ADDRESS
                            [ from ADDRESS iif STRING ]
                            [ oif STRING ] [ tos TOS ]
                            [ mark NUMBER ] [ vrf NAME ]
                            [ uid NUMBER ] [ ipproto PROTOCOL ]
                            [ sport NUMBER ] [ dport NUMBER ]
       ip route { add | del | change | append | replace } ROUTE
SELECTOR := [ root PREFIX ] [ match PREFIX ] [ exact PREFIX ]
            [ table TABLE_ID ] [ vrf NAME ] [ proto RTPROTO ]
            [ type TYPE ] [ scope SCOPE ]
ROUTE := NODE_SPEC [ INFO_SPEC ]
NODE_SPEC := [ TYPE ] PREFIX [ tos TOS ]
             [ table TABLE_ID ] [ proto RTPROTO ]
             [ scope SCOPE ] [ metric METRIC ]
             [ ttl-propagate { enabled | disabled } ]
INFO_SPEC := { NH | nhid ID } OPTIONS FLAGS [ nexthop NH ]...
NH := [ encap ENCAPTYPE ENCAPHDR ] [ via [ FAMILY ] ADDRESS ]
      [ dev STRING ] [ weight NUMBER ] NHFLAGS
FAMILY := [ inet | inet6 | mpls | bridge | link ]
OPTIONS := FLAGS [ mtu NUMBER ] [ advmss NUMBER ] [ as [ to ] ADDRESS ]
           [ rtt TIME ] [ rttvar TIME ] [ reordering NUMBER ]
           [ window NUMBER ] [ cwnd NUMBER ] [ initcwnd NUMBER ]
           [ ssthresh NUMBER ] [ realms REALM ] [ src ADDRESS ]
           [ rto_min TIME ] [ hoplimit NUMBER ] [ initrwnd NUMBER ]
           [ features FEATURES ] [ quickack BOOL ] [ congctl NAME ]
           [ pref PREF ] [ expires TIME ] [ fastopen_no_cookie BOOL ]
TYPE := { unicast | local | broadcast | multicast | throw |
          unreachable | prohibit | blackhole | nat }
TABLE_ID := [ local | main | default | all | NUMBER ]
SCOPE := [ host | link | global | NUMBER ]
NHFLAGS := [ onlink | pervasive ]
RTPROTO := [ kernel | boot | static | NUMBER ]
PREF := [ low | medium | high ]
TIME := NUMBER[s|ms]
BOOL := [1|0]
FEATURES := ecn
ENCAPTYPE := [ mpls | ip | ip6 | seg6 | seg6local | rpl | ioam6 | xfrm ]
ENCAPHDR := [ MPLSLABEL | SEG6HDR | SEG6LOCAL | IOAM6HDR | XFRMINFO ]
SEG6HDR := [ mode SEGMODE ] segs ADDR1,ADDRi,ADDRn [hmac HMACKEYID] [cleanup]
SEGMODE := [ encap | encap.red | inline | l2encap | l2encap.red ]
SEG6LOCAL := action ACTION [ OPTIONS ] [ count ]
ACTION := { End | End.X | End.T | End.DX2 | End.DX6 | End.DX4 |
            End.DT6 | End.DT4 | End.DT46 | End.B6 | End.B6.Encaps |
            End.BM | End.S | End.AS | End.AM | End.BPF }
OPTIONS := OPTION [ OPTIONS ]
OPTION := { flavors FLAVORS | srh SEG6HDR | nh4 ADDR | nh6 ADDR | iif DEV | oif DEV |
            table TABLEID | vrftable TABLEID | endpoint PROGNAME }
FLAVORS := { FLAVOR[,FLAVOR] }
FLAVOR := { psp | usp | usd | next-csid }
IOAM6HDR := trace prealloc type IOAM6_TRACE_TYPE ns IOAM6_NAMESPACE size IOAM6_TRACE_SIZE
XFRMINFO := if_id IF_ID [ link_dev LINK ]
ROUTE_GET_FLAGS := [ fibmatch ]
[erika@localhost ~]$ cd /etc/sysconfig/network-scripts/route-enp0s3
-bash: cd: /etc/sysconfig/network-scripts/route-enp0s3: No such file or directory
```
```
[erika@localhost ~]$ 👩‍💻ip route show
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 100
```

## 2. Analyse de trames
🌞Analyse des échanges ARP
* videz les tables ARP des trois noeuds
```
[erika@localhost network-scripts]$ sudo ip neigh flush all
[sudo] password for erika:
[erika@localhost network-scripts]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:06 REACHABLE
```
* effectuez un ping de john vers marcel
```
[erika@localhost network-scripts]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.27 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=2.53 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.542 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.749 ms
^C
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 0.542/1.271/2.529/0.772 ms
```

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
| ----- | ----------- | --------- | ------------------------- | -------------- | -------------------------- |
| 1     | Requête ARP | x         | `marcel` `08:00:27:9e:1d:39` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `john 08:00:27:89:02:33`                       | x              | `marcel 08:00:27:9e:1d:39`  |
| ...   | ...         | ...       | ...                       |                |                            |
| 3     | Ping        | 10.3.2.12      | `marcel 08:00:27:9e:1d:39`                       | 	10.3.1.11             | `john 08:00:27:89:02:33`                         |
| 4     | Pong        | 10.3.1.11         | `john 08:00:27:89:02:33`                        | 10.3.2.12            | `marcel 08:00:27:9e:1d:39`                         |

## 3. Accès internet  

🌞Donnez un accès internet à vos machines - config routeur

* ajoutez une carte NAT (dans l'interface de Virtualbox) en 3ème inteface sur le router pour qu'il ait un accès internet  

```
[erika@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 19.924/20.033/20.143/0.109 ms
```
* une fois que c'est fait, vérifiez qu'il a bien un accès internet  
`$ sudo firewall-cmd --add-masquerade --permanent`  
`$ sudo firewall-cmd --reload`

**🌞Donnez un accès internet à vos machines - config clients**

* ajoutez une route par défaut à john et marcel   

**JOHN**
```
[erika@localhost ~]$ sudo ip route add default via 10.3.1.254 dev enp0s3

[erika@localhost ~]$ ip r s
default via 10.3.1.254 dev enp0s3
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s3
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100
```
* vérifiez que vous avez accès internet avec un ping 
```
[erika@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=21.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=23.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=21.8 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 21.325/22.185/23.406/0.887 ms
``` 
**MARCEL**
* ajoutez une route par défaut à john et marcel   
```
[erika@localhost ~]$  sudo ip route add default via 10.3.2.254 dev enp0s3

[erika@localhost ~]$ ip r s
default via 10.3.2.254 dev enp0s3
10.3.1.0/24 via 10.3.2.254 dev enp0s3
10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 100
```
* vérifiez que vous avez accès internet avec un ping 
```
[erika@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=21.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 21.140/21.825/22.511/0.685 ms
```
* donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser  
ping vers un nom de domaine pour vérifier la résolution de nom  

**MARCEL**
```
[erika@localhost ~]$ curl gitlab.com
[erika@localhost ~]$ dig gitlab.com

; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52403
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             300     IN      A       172.65.251.78

;; Query time: 30 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 27 12:11:57 CEST 2023
;; MSG SIZE  rcvd: 55

--- 

[erika@localhost ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=55 time=19.3 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=55 time=14.6 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=3 ttl=55 time=17.7 ms
^C64 bytes from 172.65.251.78: icmp_seq=4 ttl=55 time=14.1 ms

--- gitlab.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 15183ms
rtt min/avg/max/mdev = 14.099/16.407/19.290/2.156 ms
```

**🌞Analyse de trames**

