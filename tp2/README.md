# TP2 : Ethernet, IP, et ARP
### 👩‍💻= où se trouve la réponse
## I. Setup IP
#### 🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines

#### réalisé avec une machine virtuelle
Vous renseignerez dans le compte rendu :
* les deux IPs choisies, en précisant le masque :

👩‍💻ip1 vm :  192.168.179.133  
masque ip1 vm : 255.255.255.0  
👩‍💻ip2 : 10.1.1.1  
masque ip2 : 255.255.255.252

_commande utilisée: ipcongig_
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : lan
   Adresse IPv6. . . . . . . . . . . . . .: 2001:861:3104:9a0:11fd:f9e8:ef6e:f3c9
   Adresse IPv6 temporaire . . . . . . . .: 2001:861:3104:9a0:417:bbb:a4cd:dd7a
   Adresse IPv6 de liaison locale. . . . .: fe80::f55:8a87:642e:501d%16
   👩‍💻Adresse IPv4. . . . . . . . . . . . . .: 10.1.1.1
   👩‍💻Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : fe80::4a29:52ff:fe50:bc3c%16
   ```

_commande utilisée pour l'adresse ip de la vm : ip a_

```
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:50:56:38:f2:4f brd ff:ff:ff:ff:ff:ff
    altname enp2s1
    inet 👩‍💻192.168.179.133/24 brd 192.168.179.255 scope global dynamic ens33
       valid_lft 1580sec preferred_lft 1580sec
    inet6 fe80::250:56ff:fe38:f24f/64 scope link
       valid_lft forever preferred_lft forever
```
#### 🌞 Prouvez que la connexion est fonctionnelle entre les deux machines

```
👩‍💻PS C:\Users\erika> ping  192.168.179.133

Envoi d’une requête 'Ping'  192.168.179.133 avec 32 octets de données :
Réponse de 192.168.179.133 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.179.133 : octets=32 temps<1ms TTL=64
Réponse de 192.168.179.133 : octets=32 temps<1ms TTL=64
Réponse de 192.168.179.133 : octets=32 temps<1ms TTL=64

👩‍💻Statistiques Ping pour 192.168.179.133:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
 ```

 #### 🌞 Wireshark it
 * déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping :

 type pingpong qui correspond à Echo Request et Echo réponse 

## II. ARP my bro
#### 🌞 Check the ARP table
* commande pour afficher table ARP 
```
arp -a
```

* MAC de mon binome depuis mon table ARP 
```
Interface : 192.168.179.1 --- 0x11
  Adresse Internet      Adresse physique      Type
  192.168.179.133     👩‍💻 00-50-56-38-f2-4f     
```
* déterminez la MAC de la gateway de votre réseau
```
  192.168.179.254  00-50-56-e6-93-c8     dynamique
 ```

 #### 🌞 Manipuler la table ARP
 * commande pour vider votre table ARP
 ```
arp -d 
 ```
* prouvez que ça fonctionne en l'affichant et en constatant les changements
  
  _je ne comprends pas pourquoi cela ne marche pas..._

```
PS C:\Users\erika> arp -d
La suppression de l'entrée ARP a échoué : L'opération demandée nécessite une élévation.
```
#### 🌞 Wireshark it
