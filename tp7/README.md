# II. SSH  
### 👩‍💻= où se trouve la réponse
## 1. Fingerprint
### B. Manips
**🌞 Effectuez une connexion SSH en vérifiant le fingerprint**
* en rendu je veux voir le message du serveur à la première connexion
```
$ ssh erika@10.7.1.11

👩‍💻ED25519 key fingerprint is SHA256:C85grxHi08554az3bh+0gbH36IUGWdPbY/Wza45dYL4f.
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:1: 10.6.1.101
    ~/.ssh/known_hosts:4: 10.6.1.11
    ~/.ssh/known_hosts:5: 10.6.1.254
    ~/.ssh/known_hosts:6: 10.7.1.254
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.7.1.11' (ED25519) to the list of known hosts.
```
* je veux aussi une commande qui me montre l'empreinte du serveur
```
[erika@johntp7 ~]$ ssh erika@10.7.1.11
The authenticity of host '10.7.1.11 (10.7.1.11)' can't be established.
👩‍💻ED25519 key fingerprint is SHA256:C85grxHi08554az3bh+0gbH36IUGWdPbY/Wza45dYL4f.
```
## 2. Conf serveur SSH
**🌞 Consulter l'état actuel**
* vérifiez que le serveur SSH tourne actuellement sur le port 22/tcp  
* vérifiez que le serveur SSH est disponible actuellement sur TOUTES les IPs de la machine  
* ça se fait en une seule commande ss avec les bonnes options (appelez-moi si besoin d'aide)  
