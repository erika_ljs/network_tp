# TP5 : TCP, UDP et services réseau
## 0. Prérequis
🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

déterminez, pour chaque application :

* IP et port du serveur auquel vous vous connectez
* le port local que vous ouvrez pour vous connecter  

  
- Youtube 
- [capture trafic Youtube](tp5_service_1.pcapng)
  
- Notion 
- [capture trafic Notion](tp5_service_2.pcapng)
- Discord   
 - [capture trafic Discord](tp5_service_3.pcapng)
- Visual Studio 
- [capture trafic Visual studio](tp5_service_4.pcapng)  
- Netflix
- [capture trafic netflix](tp5_service_5.pcapng)


🌞 **Demandez l'avis à votre OS**
```
PS C:\WINDOWS\system32> netstat -n -b
Connexions actives

  Proto  Adresse locale         Adresse distante       État
 [svchost.exe]
  TCP    10.33.70.195:52378     20.199.120.182:443     ESTABLISHED
 [OneDrive.exe]
  TCP    10.33.70.195:53693     52.98.206.242:443      ESTABLISHED
 [chrome.exe]
  TCP    10.33.70.195:53854     162.159.134.234:443    ESTABLISHED
 [Discord.exe]
  TCP    10.33.70.195:53857     162.159.133.232:443    ESTABLISHED
 [Discord.exe]
  TCP    10.33.70.195:53863     185.199.111.133:443    ESTABLISHED
 [chrome.exe]
  TCP    10.33.70.195:53868     95.100.200.74:443      CLOSE_WAIT
 [NortonSecurity.exe]
  TCP    10.33.70.195:53906     23.96.112.38:443       ESTABLISHED
 [NortonSecurity.exe]
  TCP    10.33.70.195:53909     20.42.73.28:443        TIME_WAIT
  TCP    10.33.70.195:53912     44.206.70.113:443      ESTABLISHED
 ```
 ## II. Setup Virtuel
 **🌞 Examinez le trafic dans Wireshark**

* déterminez si SSH utilise TCP ou UDP
```
Transmission Control Protocol, Src Port: 58078, Dst Port: 22, Seq: 0, Len: 0
```
* repérez le 3-Way Handshake à   l'établissement de la connexion
c'est le SYN SYNACK ACK
```

```




## 2. Routage
**🌞 Prouvez que**
* node1.tp5.b1 a un accès internet
```
[erika@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=19.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 18.519/19.824/21.703/1.361 ms
```
* node1.tp5.b1 peut résoudre des noms de domaine publics (comme www.ynov.com)
```
[erika@node1 ~]$ ping ynov.com
PING ynov.com (104.26.10.233) 56(84) bytes of data.
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=1 ttl=55 time=21.4 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=2 ttl=55 time=23.1 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=3 ttl=55 time=22.8 ms
^C64 bytes from 104.26.10.233: icmp_seq=4 ttl=55 time=22.1 ms

--- ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 15180ms
rtt min/avg/max/mdev = 21.379/22.350/23.107/0.664 ms
```
## 3. Serveur Web
**🌞 Installez le paquet nginx**

* avec une commande dnf install  
`sudo dnf install nginx`
**🌞 Créer le site web**
```
[erika@web ~]$ cd /var/
[erika@web var]$ sudo mkdir www
[erika@web var]$ cd www
[erika@web www]$ sudo mkdir site_web_nul
[erika@web www]$ cd site_web_nul/
[erika@web site_web_nul]$ sudo touch index.html
[erika@web site_web_nul]$ sudo nano index.html
[erika@web site_web_nul]$ sudo cat index.html
<h1>MEOW <3</h1>
```
🌞 Donner les bonnes permissions

`sudo chown -R nginx:nginx /var/www/site_web_nul`

**🌞 Créer un fichier de configuration NGINX pour notre site web**  

`cd /etc/nginx/conf.d/`

`sudo touch site_web_nul.conf`

`sudo nano site_web_nul.conf`
```
[erika@web conf.d]$ sudo cat site_web_nul.conf
server {
  # le port sur lequel on veut écouter
  listen 80;

  # le nom de la page d'accueil si le client de la précise pas
  index index.html;

  # un nom pour notre serveur (pas vraiment utile ici, mais bonne pratique)
  server_name www.site_web_nul.b1;

  # le dossier qui contient notre site web
  root /var/www/site_web_nul;
}
```
**🌞 Démarrer le serveur web !**

`sudo systemctl start nginx`
```
[erika@web ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Tue 2023-11-14 11:42:02 CET; 31s ago
    Process: 1376 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1377 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1378 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1379 (nginx)
      Tasks: 2 (limit: 4674)
     Memory: 3.2M
        CPU: 61ms
     CGroup: /system.slice/nginx.service
             ├─1379 "nginx: master process /usr/sbin/nginx"
             └─1380 "nginx: worker process"

Nov 14 11:42:02 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 14 11:42:02 web nginx[1377]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 14 11:42:02 web nginx[1377]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 14 11:42:02 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```

**🌞 Ouvrir le port firewall**
```
[erika@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[erika@web ~]$ sudo firewall-cmd --reload
success
```

